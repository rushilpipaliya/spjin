<!DOCTYPE html>
<html class="no-js" lang="zxx">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="keywords" content="SITE KEYWORDS HERE" />
    <meta name="description" content="">
    <meta name='copyright' content=''>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>SPJIN - Smt.P.J.Institute Of Nursing</title>
    <link rel="icon" type="image/png" href="images/spjin/logo-gujarati.png">
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700,900" rel="stylesheet">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/font-awesome.min.css">
    <link rel="stylesheet" href="css/jquery.fancybox.min.css">
    <link rel="stylesheet" href="css/owl.carousel.min.css">
    <link rel="stylesheet" href="css/owl.theme.default.min.css">
    <link rel="stylesheet" href="css/animate.min.css">
    <link rel="stylesheet" href="css/slicknav.min.css">
    <link rel="stylesheet" href="css/magnific-popup.css">
    <link rel="stylesheet" href="css/normalize.css">
    <link rel="stylesheet" href="style.css">
    <link rel="stylesheet" href="css/responsive.css">
    <link rel="stylesheet" href="css/w3.css">
    <link rel="stylesheet" href="css/color/color1.css">
    <link rel="stylesheet" href="#" id="colors">
</head>

<body>
    <div class="book_preload">
        <div class="book">
            <div class="book__page"></div>
            <div class="book__page"></div>
            <div class="book__page"></div>
        </div>
    </div>
    <header class="header">
        <div class="header-inner">
            <div class="container">
                <div class="row">
                    <div class="col-lg-3 col-md-3 col-12">
                        <div class="logo"><a href="index.php"><img src="images/spjin/logo-gujarati.png" alt="#"></a></div>
                        <div class="mobile-menu"></div>
                    </div>
                    <div class="col-lg-9 col-md-9 col-12">
                        <div class="header-widget">
                            <div class="single-widget"><i class="fa fa-phone"></i>
                                <h4>Call Now<span>+91 63 5566 9181</span><span>+91 63 5566 9107</span></h4>
                            </div>
                            <div class="single-widget"><i class="fa fa-envelope-o"></i>
                                <h4>Send Message<a href="mailto:info@spjinbhavnagar.org"><span>info@spjinbhavnagar.org</span><br></a></h4>
                            </div>
                            <div class="single-widget mt-10"><i class="fa fa-map-marker"></i>
                                <h4>Our Location<span>134/ 135, Sardar Patel - 2 Society , <br>Iscon Club Road, Bhavnagar</span></h4>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="header-menu">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <nav class="navbar navbar-default">
                            <div class="navbar-collapse">
                                <ul id="nav" class="nav menu navbar-nav">
                                    <li class="a1"><a href="index.php">Home</a></li>
                                    <li class="a2"><a href="">About Us<i class="fa fa-angle-down"></i></a>
                                        <ul class="dropdown">
                                            <li><a href="mission-vision.php">Mission & Vision</a></li>
                                            <li><a href="about.php">About College</a></li>
                                            <li><a href="director-message.php">Director's Message</a></li>
                                            <!--<li><a href="#">Campus Facilities</a></li>-->
                                        </ul>
                                    </li>
                                    <li class="a3"><a href="#">Departments<i class="fa fa-angle-down"></i></a>
                                        <ul class="dropdown">
                                            <li><a href="gnm.php">GNM (General Nursing Midwifery)</a></li>
                                            <li><a href="anm.php">ANM (Auxiliary Nursing Midwifery)</a></li>
                                        </ul>
                                    </li>
                                    <li class="a4"><a href="admissions.php">Admissions</a></li>
                                    <li class="a5"><a href="career.php">Career</a></li>
                                    <li class="a6"><a href="gallery.php">Gallery</a></li>
                                    <li class="a7"><a href="contact.php">Contact</a></li>
                                </ul>
                                <div class="button"><a href="contact.php" class="btn"><i class="fa fa-pencil"></i>Inquire Now</a></div>
                            </div>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </header>