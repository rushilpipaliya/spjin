
    <footer class="footer overlay section">
        <div class="footer-top">
            <div class="container">
                <div class="row">
                    <div class="col-lg-3 col-md-6 col-12">
                        <div class="single-widget about">
                            <div class="logo"><a href="index.php"><img src="images/spjin/footer-logo.png" alt="#"></a></div>
                        </div>
                    </div>
                    <div class="col-lg-1 col-md-6 col-12"></div>
                    <div class="col-lg-4 col-md-6 col-12">
                        <div class="single-widget latest-news">
                            <h2>Get in touch</h2>
                            <ul class="list">
                                <li><b><i class="fa fa-phone"></i>Phone:</b><br>&nbsp;+91 63 5566 9181 / +91 63 5566 9107</li>
                                <li><b><i class="fa fa-envelope"></i>Email:</b><br><a href="mailto:info@spjinbhavnagar.org">info@spjinbhavnagar.org</a></li>
                                <li><b><i class="fa fa-map-o"></i>Address:</b><br>134/ 135, Sardar Patel - 2 Society , Iscon Club Road, Bhavnagar</li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-lg-1 col-md-6 col-12"></div>
                    <div class="col-lg-3 col-md-6 col-12">
                        <div class="single-widget newsletter">
                            <h2>Subscribe Newsletter</h2>
                            <div class="mail">
                                <p>Don't miss to subscribe to our news feed, Get the latest updates from us!</p>
                                <div class="form"><input type="email" placeholder="Enter your email"><button class="button" type="submit"><i class="fa fa-envelope"></i></button></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="footer-bottom">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <div class="bottom-head">
                            <div class="row">
                                <div class="col-12">
                                    <ul class="social">
                                        <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                        <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                        <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                                        <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                                        <li><a href="#"><i class="fa fa-youtube"></i></a></li>
                                    </ul>
                                    <div class="copyright">
                                        <p>© Copyright 2018 <a href="#">Smt.P.J.Institute Of Nursing</a>. All Rights Reserved</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </footer>
    <script src="js/jquery.min.js"></script>
    <script src="js/jquery-migrate.min.js"></script>
    <script src="js/popper.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/colors.js"></script>
    <script src="js/jquery.stellar.min.js"></script>
    <script src="js/particles.min.js"></script>
    <script src="js/facnybox.min.js"></script>
    <script src="js/jquery.magnific-popup.min.js"></script>
    <script src="js/masonry.pkgd.min.js"></script>
    <script src="js/circle-progress.min.js"></script>
    <script src="js/owl.carousel.min.js"></script>
    <script src="js/waypoints.min.js"></script>
    <script src="js/slicknav.min.js"></script>
    <script src="js/jquery.counterup.min.js"></script>
    <script src="js/easing.min.js"></script>
    <script src="js/wow.min.js"></script>
    <script src="js/jquery.scrollUp.min.js"></script>
    <script src="http://maps.google.com/maps/api/js?key=AIzaSyC0RqLa90WDfoJedoE3Z_Gy7a7o8PCL2jw"></script>
    <script src="js/gmaps.min.js"></script>
    <script src="js/main.js"></script>
    <script src="js/map-script.js"></script>
</body>

</html>
