<?php include 'h.php'; ?>

<!-- Start Breadcrumbs -->
<section class="breadcrumbs overlay">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <h2>Contact Us</h2>
                <ul class="bread-list">
                    <li><a href="index.php">Home<i class="fa fa-angle-right"></i></a></li>
                    <li class="active"><a href="#">Contact Us</a></li>
                </ul>
            </div>
        </div>
    </div>
</section>
<!--/ End Breadcrumbs -->

<!-- Contact Us -->
<section id="contact" class="contact section">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="section-title">
                    <h2><span>Contact</span> Information</h2>
                    <p>Contact us to know more about us and about institute.</p>
                </div>
            </div>
        </div>
        <div class="contact-head">
            <div class="row">
                <div class="col-lg-6 col-md-6 col-12">
                    <div class="contact-map">
                        <!-- Map -->
                        <div id="map"></div>
                        <!--/ End Map -->
                    </div>
                </div>
                <div class="col-lg-6 col-md-6 col-12">
                    <div class="form-head">
                        <!-- Form -->
                        <form class="form" action="#">
                            <div class="form-group">
                                <input name="name" type="text" placeholder="Enter Name">
                            </div>
                            <div class="form-group">
                                <input name="email" type="email" placeholder="Email Address">
                            </div>
                            <div class="form-group">
                                <input name="mobile" type="text" placeholder="Mobile Number">
                            </div>
                            <div class="form-group">
                                <textarea name="message" placeholder="Message"></textarea>
                            </div>
                            <div class="form-group">
                                <div class="button">
                                    <button type="submit" class="btn primary">Submit</button>
                                </div>
                            </div>
                        </form>
                        <!--/ End Form -->
                    </div>
                </div>
            </div>
        </div>
        <div class="contact-bottom">
            <div class="row">
                <div class="col-lg-4 col-md-4 col-12">
                    <!-- Contact-Info -->
                    <div class="contact-info">
                        <div class="icon"><i class="fa fa-map"></i></div>
                        <h3>Location</h3>
                        <p>134/ 135, Sardar Patel - 2 Society, Iscon Club Road, Bhavnagar</p>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-12">
                    <!-- Contact-Info -->
                    <div class="contact-info">
                        <div class="icon"><i class="fa fa-envelope"></i></div>
                        <h3>Email Address</h3>
                        <a href="mailto:info@spjinbhavnagar.org">info@spjinbhavnagar.org</a>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-12">
                    <!-- Contact-Info -->
                    <div class="contact-info">
                        <div class="icon"><i class="fa fa-phone"></i></div>
                        <h3>Get in Touch</h3>
                        <p>+91 63 5566 9181</p>
                        <p>+91 63 5566 9107</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!--/ End Contact Us -->
<br>

<?php include 'f.php'; ?>
<script type="text/javascript">
    $(document).ready(function() {
        $('.a7').addClass('active');
    });
</script>
