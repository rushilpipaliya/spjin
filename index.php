<?php include 'h.php'; ?>
    <section class="home-slider">
        <div class="slider-active">
            <div class="single-slider" style="background-image:url('images/slider/slider-bg1.jpg')">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-8 col-md-8 col-12">
                            <div class="slider-text">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="single-slider" style="background-image:url('images/slider/slider-bg2.jpg')">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-8 offset-lg-2 col-md-8 offset-md-2 col-12">
                            <div class="slider-text text-center">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="single-slider" style="background-image:url('images/slider/slider-bg3.jpg')">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-8 offset-lg-4 col-md-8 offset-md-4 col-12">
                            <div class="slider-text text-right">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="our-features section">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="section-title">
                        <h2>Welcome to <span>Smt.P.J.Institute Of Nursing</span></h2>
                    </div>
                    <div class="section-title titleDesc">
                        <p>Smt.P.J.Institute of Nursing is Gujarat's only ISO 9001: 2015, ISO 14001: 2015 certified organization.</p><br>
                        <p>Smt.P.J.Institute of Nursing is established and managed by Astitva Cheritable Trust, Bhavnagar.
                           Smt.P.J.institute of Nursing is affiliated to the Gujarat Nursing Council (GNC).
                           It is one of the best institute for nursing in Gujarat.
                           All nursing programs are affiliated to Gujarat Nursing Council.
                           Smt.P.J.Institute of Nursing is licensed and accredited by the Indian Nursing Council (INC).</p><br>
                         <p>Our organization offers two courses G.N.M (General Nursing Midwifery) and A.N.M (Auxiliary Nursing Midwifery).
                           We have a team of well experienced teachers for student's education and their bright future.
                           According to the rules of government, students who are eligible for scholarship can get a scholarship.
                           Students will be prepare for competitive exams and IELTS exams with course.
                           For students we have a laboratories with all facilities and latest technologies.
                           We provide a hostel facility for needy and long distance students who wish to avail the same.</p><br>
                        <p>We gives the understudies an open door in nursing.
                           Nursing is one of the noblest calling in the world.
                           There is an incredible interest of prepared medical attendants everywhere throughout the world.
                           So as to take care of this over rising demand of medical caretakers "Smt.P.J.Institute of Nursing" is trying all endeavors to prepare the attendants to address the difficulties of the calling.</p>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="courses section-bg section">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="section-title">
                        <h2>Popular <span>Courses</span> We Offer</h2>
                        <p>Below we describe a courses which is offer by our organization.</p>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <div class="single-course">
                        <div class="course-head overlay"><img src="images/course/course1.jpg" alt="#"><a href="course-single.html" class="btn"><i class="fa fa-link"></i></a></div>
                        <div class="single-content">
                            <h4 style="text-align: center;"><a href="course-single.html">A.N.M - Auxiliary Nursing Midwifery</a></h4>
                            <p>A.N.M is a female health worker in India who is known as the first contact person between the community and the health services.</p>
                        </div>
                        <div class="course-meta">
                            <div class="meta-left"><span><i class="fa fa-users"></i>36 Seat</span><span><i class="fa fa-clock-o"></i>2 Years</span></div><!--<span class="price">$350</span>-->
                        </div>
                    </div>
                    <div class="single-course">
                        <div class="course-head overlay"><img src="images/course/course2.jpg" alt="#"><a href="course-single.html" class="btn"><i class="fa fa-link"></i></a></div>
                        <div class="single-content">
                            <h4 style="text-align: center;"><a href="course-single.html">G.N.M - General Nursing Midwifery</a></h4>
                            <p>G.N.M is concerned with the education of nurses in the areas of midwifery, nursing and general health care.</p>
                        </div>
                        <div class="course-meta">
                            <div class="meta-left"><span><i class="fa fa-users"></i>20 Seat</span><span><i class="fa fa-clock-o"></i>3 Years</span></div><!--<span class="price">$590</span>-->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="team section">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="section-title">
                        <h2>Our Awesome <span>Teachers</span></h2>
                        <p>Mauris at varius orci. Vestibulum interdum felis eu nisl pulvinar, quis ultricies nibh. Sed ultricies ante vitae laoreet sagittis. In pellentesque viverra purus. Sed risus est, molestie nec hendrerit hendreri </p>
                    </div>
                </div>
            </div>
            <div class="row owl-carousel owl-theme">
                <div class="col-lg-3 col-md-6 col-12">
                    <div class="single-team item"><img src="images/team/team1.jpg" alt="#">
                        <div class="team-hover">
                            <h4>Rohan Jonson<span>Associate Professor</span></h4>
                            <p>cumque nihil impedit quo minusid quod maxime placeat facere possimus</p>
                            <ul class="social">
                                <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                                <li><a href="#"><i class="fa fa-youtube"></i></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 col-12">
                    <div class="single-team item"><img src="images/team/team2.jpg" alt="#">
                        <div class="team-hover">
                            <h4 class="name">Ian Harvey<span class="work">Web Programmer</span></h4>
                            <p>cumque nihil impedit quo minusid quod maxime placeat facere possimus</p>
                            <ul class="social">
                                <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                                <li><a href="#"><i class="fa fa-youtube"></i></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 col-12">
                    <div class="single-team item"><img src="images/team/team3.jpg" alt="#">
                        <div class="team-hover">
                            <h4 class="name">Lusfat Roman<span class="work">Software Engineer</span></h4>
                            <p>cumque nihil impedit quo minusid quod maxime placeat facere possimus</p>
                            <ul class="social">
                                <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                                <li><a href="#"><i class="fa fa-youtube"></i></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 col-12">
                    <div class="single-team item"><img src="images/team/team4.jpg" alt="#">
                        <div class="team-hover">
                            <h4 class="name">Nalpamb Bold<span class="work">JS Developer</span></h4>
                            <p>cumque nihil impedit quo minusid quod maxime placeat facere possimus</p>
                            <ul class="social">
                                <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                                <li><a href="#"><i class="fa fa-youtube"></i></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
<?php include 'f.php'; ?>
<script type="text/javascript">
    $(document).ready(function() {
        $('.a1').addClass('active');
    });
</script>
<script type="text/javascript">
    $('.owl-carousel').owlCarousel({
        loop:true,
        margin:10
    });
</script>
