<?php include 'h.php';?>
    <!-- Start Breadcrumbs -->
    <section class="breadcrumbs overlay">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <h2>GNM (General Nursing Midwifery)</h2>
                    <ul class="bread-list">
                        <li><a href="index.php">Home<i class="fa fa-angle-right"></i></a></li>
                        <li class="active"><a href="#">GNM (General Nursing Midwifery)</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </section>
    <!--/ End Breadcrumbs -->

    <!-- Courses -->
    <section class="courses single section">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="single-main">
                        <div class="row">
                            <div class="col-12">
                                <div class="content">
                                    <div class="section-title">
                                        <h2>GNM <span>(General Nursing Midwifery)</span></h2>
                                    </div>
                                    <p>GNM known as the ‘General Nursing Midwifery’, is concerned with the education of nurses in the areas of midwifery, nursing and general health care.</p>
                                    <p>We help Students to develop their skills as students develop their education and understanding of general health care over the years they can move on to more specified areas of health care such as pharmacology, childcare, pediatrics, orthopaedics, nutrition, emergency nursing, intensive care and midwifery among other health care fields.</p>
                                    <h4 style="color: #1a5491;">OBJECTIVE</h4>
                                    <p>GNM’s may work in a variety of health care settings within both the public and private healthcare sectors and may operate in hospitals, nursing homes, hospices, the armed forces, community centers, health care facilities, and college universities among other health care settings.</p>
                                    <h4 style="color: #1a5491;">GOAL</h4>
                                    <p>Our goal is to provide nurses who focuses on care, rehabilitation or treatment of patients. The main objectives of the above mentioned actions are- prevention of diseases, rehabilitation and maintaining good health of patients and/or general public.</p>
                                </div>
                            </div>
                            <div class="col-12">
                                <div class="course-required">
                                    <h4 style="font-size: 1.5rem;">ADMISSION CRITERIA</h4>
                                    <ul>
                                        <li><span>01</span>Minimum and Maximum age for admission will be 17 and 35 years respectively on or before 31st Dec of the year of admission. For ANM/LHV, there is no age bar.</li>
                                        <li><span>02</span>Minimum education:
                                            <ul>
                                                <li><span>a</span>10+2 class (Any Stream) passed or its equivalent with aggregate of 50% marks. 5% relaxation for SC/ST candidates.</li>
                                                <li><span>b</span>Those who have cleared 10+2 vocational ANM course (revised after 2001) from the school recognized by Indian Nursing Council.</li>
                                                <li><span>c</span>ANM training i.e. 10+1 ½ years training should also have passed +2 or its equivalent examination.</li>
                                            </ul>
                                        </li>
                                        <li><span>03</span>Admission of students shall be once in a year.</li>
                                        <li><span>04</span>Students shall be medically fit.</li>
                                    </ul>
                                </div>
                                <br>
                                <h4 style="font-size: 1.5rem;color: #1a5491;">JOB PROSPECTS</h4><br>
                                <p>Handful of job profiles available in front of GNM Nurses are Staff Nurse, Home Nurse, Health Visitor, Community Health Worker. They may get job in various places like Public Health Centres, Government Hospitals, Nursing Homes, NGOs, Old Age Homes, Government Health Schemes, Private Hospitals / Clinics, Government Dispensaries etc.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--/ End Courses -->

<?php include 'f.php';?>
<script type="text/javascript">
    $(document).ready(function() {
        $('.a3').addClass('active');
    });
</script>
