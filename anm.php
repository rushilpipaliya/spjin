<?php include 'h.php';?>
<!-- Start Breadcrumbs -->
<section class="breadcrumbs overlay">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <h2>ANM (Auxiliary Nursing Midwifery)</h2>
                <ul class="bread-list">
                    <li><a href="index.php">Home<i class="fa fa-angle-right"></i></a></li>
                    <li class="active"><a href="#">ANM (Auxiliary Nursing Midwifery)</a></li>
                </ul>
            </div>
        </div>
    </div>
</section>
<!--/ End Breadcrumbs -->

<!-- Courses -->
<section class="courses single section">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="single-main">
                    <div class="row">
                        <div class="col-12">
                            <div class="content">
                                <div class="section-title">
                                    <h2>ANM <span>(Auxiliary Nursing Midwifery)</span></h2>
                                </div>
                                <p>Auxiliary Nursing Midwifery, commonly known as ANM, is a female health worker in India who is known as the first contact person between the community and the health services. ANMs are regarded as the grass-roots workers in the health organisation pyramid.</p>
                                <p>The College strives to provide persevered management in non-traditional education with an emphasis on teaching certified sensible nurses to enhance to the associate diploma registered nurse stage; to provide opportunities to men and women of numerous racial, ethnic, and socioeconomic backgrounds and to individuals who may otherwise have been excluded from profession development; to prepare graduates who advantage from their expanded degree of information; and to offer the base for in addition expert schooling.</p>
                                <h4 style="color: #1a5491;">OBJECTIVE</h4>
                                <p>To prepare an ANM who, beneath the path of a registered nurse, is ready to give basic nursing care at the network level/village with precise skills to satisfy health desires of the community. The faculty consider understanding received by the ANM nurse is basic to all nursing.</p>
                                <h4 style="color: #1a5491;">GOAL</h4>
                                <p>To get ready gifted and viable FHW to accomplish the objectives of National Rural Health Mission that goes for achieving sensational enhancements in the wellbeing framework and wellbeing status of the nation.</p>
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="course-required">
                                <h4 style="font-size: 1.5rem;">ADMISSION CRITERIA</h4>
                                <ul>
                                    <li><span>01</span>The minimum age for admission is 17 years on or before 31st Dec. of the year in which admission is sought.</li>
                                    <li><span>02</span>The maximum age for admission shall be 35 years.</li>
                                    <li><span>03</span>The minimum educational requirements shall be the passing of 10+2 Class (any Stream) from central board secondary education or a recognized equivalent public examination. Subjects of study must be equivalent to those prescribed by the CBSE for the class XII with minimum 50 %.</li>
                                    <li><span>04</span>Admission is subjected to satisfactory, medical examination report.</li>
                                </ul>
                            </div>
                            <br>
                            <h4 style="font-size: 1.5rem;color: #1a5491;">JOB PROSPECTS</h4><br>
                            <p>Incredible accentuation has been laid on improving wellbeing and prosperity at the grass roots level by the Govt of India. National Rural Health Mission was made in 2005 to meet individuals' wellbeing in country zones. ANM/FHW will assume a key job in usage of the NRHM approaches. Vocation choices incorporate Midwife Associates in doctor's facilities, Female Health Workers, Multiple Health Workers in Hospitals, Primary Health Centers, Clinics and comparative establishments.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!--/ End Courses -->

<?php include 'f.php';?>
<script type="text/javascript">
    $(document).ready(function() {
        $('.a3').addClass('active');
    });
</script>
