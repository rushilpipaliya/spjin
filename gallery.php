<?php include 'h.php'; ?>
<link href="css/lightgallery.css">
<link href="pagination/css/pagination.css" rel="stylesheet" type="text/css" />
<link href="pagination/css/A_green.css" rel="stylesheet" type="text/css" />
<!-- Start Breadcrumbs -->
<section class="breadcrumbs overlay">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <h2>Gallery</h2>
                <ul class="bread-list">
                    <li><a href="index.php">Home<i class="fa fa-angle-right"></i></a></li>
                    <li class="active"><a href="#">Gallery</a></li>
                </ul>
            </div>
        </div>
    </div>
</section>
<!--/ End Breadcrumbs -->

<!-- Events -->
<section class="events archives section">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="section-title">
                    <h2><span>Gallery</span></h2>
                    <p>Mauris at varius orci. Vestibulum interdum felis eu nisl pulvinar, quis ultricies nibh. Sed ultricies ante vitae laoreet sagittis. In pellentesque viverra purus. Sed risus est, molestie nec hendrerit hendreri </p>
                </div>
            </div>
        </div>
        <div class="row" id="lightgallery">
            <?php
            $conn=mysqli_connect('localhost','root','root','spjin');
            include("pagination/function.php");
            $page = (int) (!isset($_GET["page"]) ? 1 : $_GET["page"]);
            $limit = 9;
            $startpoint = ($page * $limit) - $limit;
            $statement = "tbl_image";

            $res = mysqli_query($conn, "select * from $statement LIMIT $startpoint, $limit");
            while($row=mysqli_fetch_array($res)){
                ?>
                <div class="col-lg-4 col-md-6 col-12" data-responsive="images/gallery/<?php echo $row['imageName'];  ?>.jpg 375, images/gallery/<?php echo $row['imageName'];  ?>.jpg 480, images/gallery/<?php echo $row['imageName'];  ?>.jpg 800" data-src="images/gallery/<?php echo $row['imageName'];  ?>.jpg">
                    <div class="single-event">
                        <a href=""><img src="images/gallery/thumbnail/<?php echo $row['imageName']; ?>.jpg?1" alt="Image"></a>
                    </div>
                    <!-- Single Event -->
                    <!--<div class="single-event">
                        <div class="head overlay">
                            <img src="images/gallery/<?php /*echo $row['imageName']; */?>.JPG" alt="#">
                            <a href="#" class="btn"><i class="fa fa-search"></i></a>
                        </div>
                    </div>-->
                    <!--/ End Single Event -->
                </div>
                <!--<div class="col-md-4" data-responsive="gallery/<?php /*echo $row['imagename'];  */?> 375, gallery/<?php /*echo $row['imagename'];  */?> 480, gallery/<?php /*echo $row['imagename'];  */?> 800" data-src="gallery/<?php /*echo $row['imagename'];  */?>">
                    <a href=""><img src="gallery/<?php /*echo $row['imagename']; */?>?1" alt="Image"></a>
                </div>-->
                <?php
            }
            ?>
        </div>
        <div id="pagingg" align="right">
            <?php echo pagination($statement,$limit,$page); ?>
        </div>
    </div>
</section>
<!--/ End Events -->

<?php include 'f.php'; ?>
<script src="js/lightgallery-all.js"></script>
<script src="js/picturefill.min.js"></script>
<script src="js/jquery.mousewheel.min.js"></script>
<script type="text/javascript">
    $(document).ready(function() {
        $('.a6').addClass('active');
        $('#lightgallery').lightGallery();
    });
    $('#mylist').change(function() {
        if ($(this).val() == '1') {
            $('.addfield').append('<input id="myInput" type="text" class="form-control" placeholder="Enter Other Field"/>');
        } else {
            $('#myInput').remove();
        }
    });

</script>
