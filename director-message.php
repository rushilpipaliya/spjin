<?php include 'h.php'; ?>

<!-- Start Breadcrumbs -->
<section class="breadcrumbs overlay">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <h2>Director's Message</h2>
                <ul class="bread-list">
                    <li><a href="index.php">Home<i class="fa fa-angle-right"></i></a></li>
                    <li class="active"><a href="#">Director's Message</a></li>
                </ul>
            </div>
        </div>
    </div>
</section>
<!--/ End Breadcrumbs -->

<!-- Courses -->
<section class="courses single section">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="single-main">
                    <div class="row">
                        <div class="col-12">
                            <div class="content">
                                <div class="section-title">
                                    <h2><span>Director's Message</span></h2>
                                </div>
                                <div class="col-12">
                                    <div class="col-lg-2 col-md-2 col-12" style="float: left;">
                                        <img src="images/gallery/43_image_small.jpg">
                                    </div>
                                </div>
                                <p style="margin-left: 3%;">Smt.P.J.Institute Of Nursing was established in 2018. Then, as now, students learned to practice professional nursing with compassion, excellence, vision, enthusiasm and integrity. Smt.P.J.Institute Of Nursing continues to educate excellent nurses who share these same values. Since our profession is ultimately about caring for people, we make sure our students get as much hands-on patient care experience as possible. The College is fortunate to be affiliated with multiple health care facilities, so when our students go into their clinical rotations, they are learning alongside the best nurses in their chosen field. </p>
                                <p style="margin-left: 3%;">Professional excellence aside, students often say that what they like most about Smt.P.J.Institute Of Nursing is the strong sense of community. I agree. The faculty, staff and I put a lot of effort into helping our students succeed: flexible degree programs, comprehensive career and academic counseling, and affordability are just some of the things that add value to a Smt.P.J.Institute Of Nursing's education. </p>
                                <p style="margin-left: 3%;">Nursing is an immensely satisfying career. It will allow you to serve others, gain knowledge and develop as a person and a professional. I invite you to find out what thousands of other working nurses and students already know - that a passion for excellence and caring is what makes Smt.P.J.Institute Of Nursing, and our students, among the very best.</p>
                                <ul style="margin-left: 3%;">
                                    <li style="font-weight: bold;">Jodhani Reena D., BSC Nursing</li>
                                    <li>Principal at Smt.P.J.Institute Of Nursing</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!--/ End Courses -->

<?php include 'f.php'; ?>
<script type="text/javascript">
    $(document).ready(function() {
        $('.a2').addClass('active');
    });
</script>