<?php include 'h.php'; ?>

<!-- Start Breadcrumbs -->
<section class="breadcrumbs overlay">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <h2>Career</h2>
                <ul class="bread-list">
                    <li><a href="index.php">Home<i class="fa fa-angle-right"></i></a></li>
                    <li class="active"><a href="#">Career</a></li>
                </ul>
            </div>
        </div>
    </div>
</section>
<!--/ End Breadcrumbs -->

<!-- Contact Us -->
<section id="contact" class="contact section">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="section-title">
                    <h2><span>Career</span></h2>
                    <p>We offer unique opportunities for both personal and professional development, along with several benefits and a great friendly work culture. With us, you can shape your career path.</p>
                </div>
            </div>
        </div>
        <div class="contact-head"">
            <div class="row">
                <div class="col-lg-8 col-md-8 col-12">
                    <div class="form-head">
                        <!-- Form -->
                        <form class="form" action="#">
                            <div class="form-group">
                                <input name="name" type="text" placeholder="Enter Name">
                            </div>
                            <div class="form-group">
                                <input name="email" type="email" placeholder="Email Address">
                            </div>
                            <div class="form-group">
                                <input name="mobile" type="text" placeholder="Mobile Number">
                            </div>
                            <div class="form-group">
                                <input name="specialization" type="text" placeholder="Specialization">
                            </div>
                            <div class="form-group customfile-wrap">
                                <input name="resume" id="upload" type="file" placeholder="Upload a Resume" readonly>
                            </div>
                            <div class="form-group">
                                <textarea name="message" placeholder="Message"></textarea>
                            </div>
                            <div class="form-group">
                                <div class="button" align="center">
                                    <button type="submit" class="btn primary">Submit</button>
                                </div>
                            </div>
                        </form>
                        <!--/ End Form -->
                    </div>
                 </div>
            </div>
        </div>
    </div>
</section>
<!--/ End Contact Us -->
<br>
<?php include 'f.php'; ?>
<script type="text/javascript">
    ;(function( $ ) {

        // Browser supports HTML5 multiple file?
        var multipleSupport = typeof $('<input/>')[0].multiple !== 'undefined',
            isIE = /msie/i.test( navigator.userAgent );

        $.fn.customFile = function() {

            return this.each(function() {

                var $file = $(this).addClass('customfile'), // the original file input
                    $wrap = $('<div class="customfile-wrap">'),
                    $input = $('<input type="text" class="customfile-filename" id="upload" value="Upload Resume" name="resume" readonly/>'),
                    // Button that will be used in non-IE browsers
                    $button = $('<button type="button" class="customfile-upload">Choose File</button>'),
                    // Hack for IE
                    $label = $('<label class="customfile-upload" for="'+ $file[0].id +'">Choose File</label>');

                // Hide by shifting to the left so we
                // can still trigger events
                $file.css({
                    position: 'absolute',
                    left: '-9999px'
                });

                $wrap.insertAfter( $file )
                    .append( $file, $input, ( isIE ? $label : $button ) );

                // Prevent focus
                $file.attr('tabIndex', -1);
                $button.attr('tabIndex', -1);

                $button.click(function () {
                    $file.focus().click(); // Open dialog
                });

                $file.change(function() {

                    var files = [], fileArr, filename;

                    // If multiple is supported then extract
                    // all filenames from the file array
                    if ( multipleSupport ) {
                        fileArr = $file[0].files;
                        for ( var i = 0, len = fileArr.length; i < len; i++ ) {
                            files.push( fileArr[i].name );
                        }
                        filename = files.join(', ');

                        // If not supported then just take the value
                        // and remove the path to just show the filename
                    } else {
                        filename = $file.val().split('\\').pop();
                    }

                    $input.val( filename ) // Set the value
                        .attr('title', filename) // Show filename in title tootlip
                        .focus(); // Regain focus

                });

                $input.on({
                    blur: function() { $file.trigger('blur'); },
                    keydown: function( e ) {
                        if ( e.which === 13 ) { // Enter
                            if ( !isIE ) { $file.trigger('click'); }
                        } else if ( e.which === 8 || e.which === 46 ) { // Backspace & Del
                            // On some browsers the value is read-only
                            // with this trick we remove the old input and add
                            // a clean clone with all the original events attached
                            $file.replaceWith( $file = $file.clone( true ) );
                            $file.trigger('change');
                            $input.val('');
                        } else if ( e.which === 9 ){ // TAB
                            return;
                        } else { // All other keys
                            return false;
                        }
                    }
                });

            });

        };

        // Old browser fallback
        if ( !multipleSupport ) {
            $( document ).on('change', 'input.customfile', function() {

                var $this = $(this),
                    // Create a unique ID so we
                    // can attach the label to the input
                    uniqId = 'customfile_'+ (new Date()).getTime(),
                    $wrap = $this.parent(),

                    // Filter empty input
                    $inputs = $wrap.siblings().find('.customfile-filename')
                        .filter(function(){ return !this.value }),

                    $file = $('<input type="file" id="'+ uniqId +'" name="'+ $this.attr('name') +'"/>');

                // 1ms timeout so it runs after all other events
                // that modify the value have triggered
                setTimeout(function() {
                    // Add a new input
                    if ( $this.val() ) {
                        // Check for empty fields to prevent
                        // creating new inputs when changing files
                        if ( !$inputs.length ) {
                            $wrap.after( $file );
                            $file.customFile();
                        }
                        // Remove and reorganize inputs

                    } else {
                        $inputs.parent().remove();
                        // Move the input so it's always last on the list
                        $wrap.appendTo( $wrap.parent() );
                        $wrap.find('input').focus();
                    }
                }, 1);

            });
        }
        $('#upload').customFile();
    }( jQuery ));
</script>
<script type="text/javascript">
    $(document).ready(function() {
        $('.a5').addClass('active');
    });
</script>
