<?php include 'h.php'; ?>

<!-- Start Breadcrumbs -->
<section class="breadcrumbs overlay">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <h2>Admissions</h2>
                <ul class="bread-list">
                    <li><a href="index.php">Home<i class="fa fa-angle-right"></i></a></li>
                    <li class="active"><a href="#">Admissions</a></li>
                </ul>
            </div>
        </div>
    </div>
</section>
<!--/ End Breadcrumbs -->

<!-- Courses -->
<section class="courses single section">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="single-main">
                    <div class="row">
                        <div class="col-12">
                            <div class="content">
                                <div class="section-title">
                                    <h2><span>Admissions</span></h2>
                                </div>
                                <h4 style="color: #1a5491;">How To Apply</h4>
                                <p>Smt.P.J.Institute Of Nursing is the place to start your education for a dream career. Your future is here for the making. The world class facilities and infrastructure are incomparable. As for the faculty, they form the driving force. So, make a beginning with Smt.P.J.Institute Of Nursing to discover the full potential in you and the exciting opportunities that lie ahead. Salokaya encourages and sustains an exemplary culture of academic excellence.</p>
                                <h4 style="color: #1a5491;">Application Procedure</h4>
                                <p>Visit/Write/Call to Smt.P.J.Institute Of Nursing at the following address to request for application form.</p>
                                <ul>
                                    <li>Director, Admissions</li>
                                    <li>Smt.P.J.Institute Of Nursing</li>
                                    <li>134/ 135, Sardar Patel - 2 Society ,</li>
                                    <li>Iscon Club Road, Bhavnagar</li>
                                    <li>Phone : +91 63 5566 9181 / +91 63 5566 9107</li>
                                    <li>Email : info@spjinbhavnagar.org</li>
                                </ul>
                                <h4 style="color: #1a5491;">List Of Documents</h4>
                                <div class="course-required">
                                    <ul>
                                        <li><span>01</span>Marksheets of Higher Secondary(10+2) and Secondary (10) examination.</li>
                                        <li><span>02</span>Birth Certificate Or School Leaving Certificate.</li>
                                        <li><span>03</span>3 Passport Sized Photographs.</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!--/ End Courses -->

<?php include 'f.php'; ?>
<script type="text/javascript">
    $(document).ready(function() {
        $('.a4').addClass('active');
    });
</script>