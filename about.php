<?php include 'h.php'; ?>
    <!-- Start Breadcrumbs -->
    <section class="breadcrumbs overlay">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <h2>About Us</h2>
                    <ul class="bread-list">
                        <li><a href="index.php">Home<i class="fa fa-angle-right"></i></a></li>
                        <li class="active"><a href="#">About Us</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </section>
    <!--/ End Breadcrumbs -->

    <!-- Courses -->
    <section class="courses single section">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="single-main">
                        <div class="row">
                            <div class="col-12">
                                <div class="content">
                                    <div class="section-title">
                                        <h2>About <span>Smt.P.J.Institute of Nursing</span></h2>
                                    </div>
                                    <p>Smt.P.J.Institute of Nursing is Gujarat's only ISO 9001: 2015, ISO 14001: 2015 certified organization.</p>
                                    <p>Smt.P.J.Institute of Nursing is established and managed by Astitva Cheritable Trust, Bhavnagar.
                                        Smt.P.J.institute of Nursing is affiliated to the Gujarat Nursing Council (GNC).
                                        It is one of the best institute for nursing in Gujarat.
                                        All nursing programs are affiliated to Gujarat Nursing Council.
                                        Smt.P.J.Institute of Nursing is licensed and accredited by the Indian Nursing Council (INC).</p>
                                    <p>Our organization offers two courses G.N.M (General Nursing Midwifery) and A.N.M (Auxiliary Nursing Midwifery).
                                        We have a team of well experienced teachers for student's education and their bright future.
                                        According to the rules of government, students who are eligible for scholarship can get a scholarship.
                                        Students will be prepare for competitive exams and IELTS exams with course.
                                        For students we have a laboratories with all facilities and latest technologies.
                                        We provide a hostel facility for needy and long distance students who wish to avail the same.</p>
                                    <p>We gives the understudies an open door in nursing.
                                        Nursing is one of the noblest calling in the world.
                                        There is an incredible interest of prepared medical attendants everywhere throughout the world.
                                        So as to take care of this over rising demand of medical caretakers "Smt.P.J.Institute of Nursing" is trying all endeavors to prepare the attendants to address the difficulties of the calling.</p>
                                </div>
                            </div>
                            <div class="col-12" style="margin-top: 3%;">
                                <div class="vision">
                                    <ul>
                                        <li><span>&drbkarow;</span><b>Society</b> is comprised of individuals from diverse backgrounds. The College of Nursing contributes to society by preparing graduates who advocate for health promotion, disease prevention, and treatment of acute and chronic illness in a global health care environment. Understanding and advocating for the needs of individuals, their families, and the community are essential in promoting active participation in self-care management.</li>
                                        <li><span>&drbkarow;</span><b>Health</b> is a dynamic state of healing; one of optimal physiological, psychosocial, and spiritual well-being. An individual’s personal definition of health is influenced by age, gender, cultural factors, and experience. An individual’s participation in self-care and health promotion behaviors is affected by his/her personal definition of health.</li>
                                        <li><span>&drbkarow;</span><b>Nursing</b> is an art and a science with a unique body of knowledge and skills. Caring is the foundation for all nurse- patient relationships. Nurses are responsible for promoting and maintaining a safe environment while embracing relationship-based care.</li>
                                        <li><span>&drbkarow;</span><b>Learning</b> is a dynamic life-long process proceeding from simple to complex allowing individuals to develop their human potential. We believe that correlation of classroom theory with clinical experience is essential. Educational activities in a student-centered environment encourage collaboration among students, faculty, and other health care professionals.</li>
                                        <li><span>&drbkarow;</span><b>Nursing Education</b> prepares the student to meet health care needs using the nursing process, applying critical thinking, evidence-based nursing, and clinical reasoning. Innovation provides the framework for educating students to become caring professional nurses exhibiting integrity, clinical competence, effective communication, advocacy, and ethical behavior. We strive for excellence in nursing education and prepare our graduates to be nursing leaders of tomorrow.</li>
                                        <li><span>&drbkarow;</span><b>Faculty</b> are committed to nursing education. Their role is to facilitate learning in a student centered culture of caring, trust, and openness, while responding to different learning styles and developmental levels. Faculty serve as role models to promote professional socialization and lifelong learning. Faculty demonstrate leadership and service in a variety of professional and community roles.</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--/ End Courses -->
<?php include 'f.php'; ?>
<script type="text/javascript">
    $(document).ready(function() {
        $('.a2').addClass('active');
    });
</script>
