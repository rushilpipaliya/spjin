<?php include 'h.php'; ?>
    <!-- Start Breadcrumbs -->
    <section class="breadcrumbs overlay">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <h2>Mission & Vision</h2>
                    <ul class="bread-list">
                        <li><a href="index.php">Home<i class="fa fa-angle-right"></i></a></li>
                        <li class="active"><a href="#">Mission & Vision</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </section>
    <!--/ End Breadcrumbs -->

    <!-- Courses -->
    <section class="courses single section">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="single-main">
                        <div class="row">
                            <div class="col-12">
                                <div class="content">
                                    <div class="section-title">
                                        <h2><span>Mission & Vision</span></h2>
                                    </div>
                                    <h4 style="color: #1a5491;">MISSION</h4><br>
                                    <p>The mission of Smt.P.J.Institute Of Nursing is to enhance the health of the community by providing excellence in nursing education, nursing research and nursing practice. We will advance the profession of nursing and serve as an advocate for optimal healthcare for all people.</p>
                                    <p>The mission will be accomplished through our strategic priorities:</p>
                                    <h6>Education: </h6><p>Provide a balanced array of educational programs to prepare nurse leaders for practice, research, and education.</p>
                                    <h6>Research: </h6><p>Increase research activity in focused areas of excellence and expand team science.</p>
                                    <h6>Practice: </h6><p>Lead nursing practice partnerships that translate nursing science into practice to improve health outcomes.</p>
                                    <hr>
                                    <div class="vision">
                                        <h4 style="color: #1a5491;">VISION</h4><br>
                                        <p>Smt.P.J.Institute of Nursing is committed to sustaining Excellence through following vision:</p>
                                        <ul>
                                            <li><span>&drbkarow;</span>Ensure high quality of nursing and healthcare education for candidates who opt for the profession in India and uplift the quality of healthcare services domestically.</li>
                                            <li><span>&drbkarow;</span>Disseminate knowledge by educating the public and professionals through strategies that include and engage diverse learners.</li>
                                            <li><span>&drbkarow;</span>Advance and preserve knowledge about human health ecology, including human responses to health and illness of individuals, families, groups, and communities.</li>
                                        </ul>
                                    </div>
                                    <hr>
                                    <div class="vision">
                                        <h4 style="color: #1a5491;">VALUES</h4><br>
                                        <p>Smt.P.J.Institute of Nursing believes :</p>
                                        <ul>
                                            <li><span>&drbkarow;</span>All people should be treated with respect, dignity, and compassion.</li>
                                            <li><span>&drbkarow;</span>Caring relationships are the core of nursing practice.</li>
                                            <li><span>&drbkarow;</span>The profession of nursing contributes to the health and well-being of individuals, families, organizations and communities.</li>
                                            <li><span>&drbkarow;</span>High-quality education, which includes both face-to-face and online learning, transforms lives.</li>
                                            <li><span>&drbkarow;</span>Students should be prepared to actively participate in a global community.</li>
                                            <li><span>&drbkarow;</span>Nursing practice and education should occur in a diverse and inclusive environment.</li>
                                            <li><span>&drbkarow;</span>Our tradition of service learning, community engagement and leadership provides a model for transforming the health of the region, nation and the world.</li>
                                            <li><span>&drbkarow;</span>Knowledge development and dissemination are our responsibility and commitment.</li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--/ End Courses -->
<?php include 'f.php'; ?>
<script type="text/javascript">
    $(document).ready(function() {
        $('.a2').addClass('active');
    });
</script>
